<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'AuthController@login');
Route::post('signup', 'AuthController@signup');

Route::middleware('auth:api')->group(function () {
    Route::resource('ticket', 'TicketController');
    Route::get('request-token', 'TicketController@requestNewTicker');
    Route::get('logout', 'AuthController@logout');
    Route::get('users', 'UserController@index');
});
