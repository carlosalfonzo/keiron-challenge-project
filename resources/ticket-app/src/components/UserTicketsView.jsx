import React, { useEffect } from 'react';
import './css/UserTicketsView.css'
import Axios from '../utils/Axios';
import Loader from './common/Loader';
import Button from './common/Button';
import TicketList from '../containers/TicketListContainer';

export default function UserTicketsView(props) {
    const {
        requestTickets,
        userRequestForTicket,
        loading,
        requestingTicket,
        error,
        match: {
            params,
            url
        },
    } = props;
    useEffect(requestUserTickets, [url]);

    function requestUserTickets() {
        const request = new Axios({
            'url': 'ticket',
            params: {
                page: params.page ? params.page : 1
            }
        });
        requestTickets(request.send());
    }

    function requestNewTicket() {
        if (!requestingTicket) {
            const newTicket = new Axios({
                'url': 'request-token'
            });
            userRequestForTicket(newTicket.send());
        }
    }

    return (
        <div className='full flex-center wrap'>
            <p className='my-tickets-title full higthlight-color bold'>My Tickets</p>
            {
                loading ? <Loader /> : (
                    <div className='user-ticket-list-and-option-container flex wrap max-width'>
                        <div className='request-button-container full flex'>
                            {
                                requestingTicket ? <Loader /> : <Button className='request-ticket-button' onClick={requestNewTicket} text='Request New Ticket' />
                            }
                        </div>
                        <TicketList user />
                    </div>
                )
            }
            {
                error && <p className='full error-message-tickers'>{error}</p>
            }
        </div>
    )
}