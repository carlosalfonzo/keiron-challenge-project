import React, { useEffect, useState } from 'react';
import Axios from '../utils/Axios';
import Loader from './common/Loader';
import Button from './common/Button';

export default function SelectUser(props) {
    const {
        loading,
        users,
        error,
        requestUsers,
        onSelectedUser,
        nextStep
    } = props;

    useEffect(getUsers, []);
    const [selectedId, setSelectedId] = useState(undefined);

    function getUsers() {
        if (!loading && !users.length) {
            const request = new Axios({
                'url': 'users'
            });
            requestUsers(request.send());
        }
    }

    function selectUser(id, name) {
        return function () {
            if (selectedId === id) {
                setSelectedId(undefined);
                return onSelectedUser({ undefined, undefined });
            }
            setSelectedId(id);
            onSelectedUser({
                user_id: id,
                selected_name: name
            });
        };
    }

    function renderUserSelectPanel() {
        return (
            <div className='full select-user-panel-container'>
                <p className='step-title bold'>Select User</p>
                <div className='border border-radius flex users-list-container'>
                    <div className='flex column full users-list-scroll-container'>
                        {
                            users.length ? (
                                users.map(({ id, name, email }) => {
                                    return (
                                        <div
                                            className={`user-row flex-between full ${selectedId === id && 'second-background'}`}
                                            key={`user-${id}`}
                                            onClick={selectUser(id, name)}
                                        >
                                            <div className='user-attribute'>
                                                <p className='bold'>Name:</p>
                                                <p className='overflow-ellipsis'>{name}</p>
                                            </div>
                                            <div className='user-attribute'>
                                                <p className='bold higthlight-color'>Email:</p>
                                                <p className='overflow-ellipsis'>{email}</p>
                                            </div>
                                        </div>
                                    );
                                })
                            ) : <p className='higthlight-color empty-users'>No users Available</p>
                        }
                    </div>
                </div>
                {
                    selectedId && (
                        <div className='flex-center users-buttons-continer'>
                            <Button
                                className='go-ahead-button'
                                onClick={nextStep}
                                text='Next Step'
                            />
                        </div>
                    )
                }
            </div>
        );
    }

    return (
        <div className='max-width full flex wrap'>
            {
                loading ? <Loader /> :
                    error ? (
                        <div className='full flex-center wrap'>
                            <p className='full error-users-message'>An error ocurred when requesting users, try again</p>
                            <Button
                                className='try-again-users'
                                onClick={getUsers}
                                text='Try Again'
                            />
                        </div>
                    ) : renderUserSelectPanel()
            }
        </div>
    )
}