import React, { useEffect } from 'react';
import './css/AdminTicketsView.css';
import { ReactComponent as Update } from '../assets/icons/update.svg';
import Axios from '../utils/Axios';
import Loader from './common/Loader';
import Button from './common/Button';
import TicketList from '../containers/TicketListContainer';

export default function UserTicketsView(props) {
    const {
        requestTickets,
        loading,
        error,
        history: {
            push
        },
        match: {
            params,
            url
        },
        refreshable,
    } = props;
    useEffect(requestUserTickets, [url]);

    function requestUserTickets() {
        const request = new Axios({
            'url': 'ticket',
            params: {
                page: params.page ? params.page : 1
            }
        });
        requestTickets(request.send());
    }

    function gotoCreate() {
        push('/create');
    }

    return (
        <div className='full flex-center wrap'>
            <p className='my-tickets-title full higthlight-color bold'>Tickets</p>
            {
                loading ? <Loader /> : (
                    <div className='user-ticket-list-and-option-container flex wrap max-width'>
                        <div className='create-button-container flex'>
                            <Button
                                className='create-ticket-button'
                                onClick={gotoCreate}
                                text='New Ticket'
                            />
                            {
                                refreshable && (
                                    <div className='border-radius flex-center border strong-background refresh-button' onClick={requestUserTickets}>
                                        <Update className='icon-color refresh-list-icon' />
                                    </div>
                                )
                            }
                        </div>
                        <TicketList admin />
                    </div>
                )
            }
            {
                error && <p className='full error-message-tickers'>{error}</p>
            }
        </div>
    )
}