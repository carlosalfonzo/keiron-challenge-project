import React, { Fragment } from 'react';
import { ReactComponent as Ticket } from '../assets/icons/ticket.svg';
import { ReactComponent as Trash } from '../assets/icons/trash.svg';
import { formatTimeStamp } from '../utils/functions';
import Axios from '../utils/Axios';
import Loader from './common/Loader';

export default function TicketListRow(props) {
	const {
		ticket: {
			loading,
			message,
			error,
			id,
			requested_ticket,
			created_at,
			user
		},
		index,
		deleteTicket,
		isAdmin
	} = props;

	function attempDeleteTicket() {
		if (isAdmin) {
			const request = new Axios({
				url: `ticket/${id}`,
				method: 'delete'
			});
			deleteTicket(request.send(), index);
		}
	}

	function renderReguarRow() {
		return (
			<Fragment>
				<Ticket className='icon-color ticket-icon' />
				<div className='attribute-container'>
					<p className='bold attribute-label'>ID</p>
					<p className='attribute-value higthlight-color'>{id}</p>
				</div>
				<div className='attribute-container'>
					<p className='bold attribute-label'>{isAdmin ? 'Requested by user:' : 'Requested:'}</p>
					<p className='attribute-value'>{requested_ticket ? 'Yes' : 'No'}</p>
				</div>
				<div className='attribute-container'>
					<p className='bold attribute-label'>Created</p>
					<p className='attribute-value'>{formatTimeStamp(created_at)}</p>
				</div>
				{
					isAdmin && (
						<Fragment>
							<div className='attribute-container'>
								<p className='bold attribute-label'>User Id</p>
								<p className='attribute-value'>{user.id}</p>
							</div>
							<div className='attribute-container'>
								<p className='bold attribute-label'>User email</p>
								<p className='attribute-value overflow-ellipsis'>{user.email}</p>
							</div>
							<Trash className='icon-color trash-icon' onClick={attempDeleteTicket} />
						</Fragment>
					)
				}
			</Fragment>
		);
	}

	function renderMessage() {
		return (
			<p className='delete-message-container full light higthlight-color'>{message ? message : 'An error ocurred when deleteing ticket, refresg the list and try again'}</p>
		);
	}

	return (
		<div className={`ticket-row-container full flex-between ${isAdmin && 'adminRow'}`}>
			{
				loading ? (
					<div className='full flex-center'>
						<Loader />
					</div>
				) : (message || error) ? renderMessage() : renderReguarRow()
			}
		</div>
	);
}