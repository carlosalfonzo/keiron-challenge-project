import React, { useEffect, Fragment } from 'react';
import { getSessionFromLocalStorage, clearSessionFromLocalStorage } from '../utils/functions';
import { ReactComponent as LogOut } from '../assets/icons/logout.svg';
import Axios from '../utils/Axios';
import Loader from './common/Loader';
import './css/Layout.css';
import { goTo } from '../routes/SetAppRoutes';

export default function Layout(props) {
  const {
    logedUser,
    setUserData,
    children,
    logOutAttempt,
    loading
  } = props;
  useEffect(checkUserInLocalStorage, []);

  function checkUserInLocalStorage() {
    if (localStorage.getItem('ticket-user')) {
      const userData = getSessionFromLocalStorage();
      setUserData(userData);
      Axios.authToken = userData.token;
    }
  }

  function logOut() {
    if (!loading) {
      const request = new Axios({
        url: 'logout'
      }).send();
      logOutAttempt(request);
      request.finally(
        () => {
          clearSessionFromLocalStorage();
          goTo('/');
        }
      );
    }
  }

  function setLayoutToRender() {
    if (logedUser) {
      return (
        <div className='full flex wrap'>
          {children}
          <div className='logout-container flex second-background wrap'>
            {
              loading ? <Loader /> : (
                <Fragment>
                  <div className='username-container flex'>
                    <p>User:</p>
                    <p className='higthlight-color username overflow-ellipsis'>{logedUser.email}</p>
                  </div>
                  <div className='full flex'>
                    <LogOut className='log-out-icon icon-color' onClick={logOut} />
                    <p className='second-color' onClick={logOut}>Log Out</p>
                  </div>
                </Fragment>
              )
            }
          </div>
        </div>
      );
    }
    return children;
  }

  return setLayoutToRender();
}