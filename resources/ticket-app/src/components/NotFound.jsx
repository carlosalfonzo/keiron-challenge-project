import React from 'react';
import './css/NotFound.css'
import Button from './common/Button';
import { goTo } from '../routes/SetAppRoutes';

export default function NotFound() {
  return (
    <div className='not-found-wrapper full flex-center'>
      <div className='not-found-container'>
        <p className='bold text-404 full alternative-color'>404</p>
        <p className='not-found-message higthlight-color full'>Page Not Found</p>
        <div className='full flex-center go-home-buttom'>
          <Button onClick={() => goTo('/')} text='Go Home' />
        </div>
      </div>
    </div>
  )
}