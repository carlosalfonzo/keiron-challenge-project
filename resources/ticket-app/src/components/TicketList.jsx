import React, { Fragment } from 'react';
import './css/TicketList.css';
import Pagination from '../containers/PaginationContainer';
import TicketListRow from '../containers/TicketListRowContainer';

export default function TicketList(props) {
    const {
        tickets,
        admin
    } = props;

    return (
        <Fragment>
            <div className='full flex ticketlist-container'>
                {
                    tickets.length ? (
                        <div className='full scroll-container flex wrap border border-radius'>
                            {
                                tickets.map(
                                    (ticketEl, index) =>
                                        <TicketListRow
                                            key={`ticket-row-${index}`}
                                            ticket={ticketEl}
                                            isAdmin={admin}
                                            index={index}
                                        />
                                )
                            }
                        </div>
                    ) : <p className='bold full empty-tickets-message'>No Tickets Found</p>
                }
            </div>
            {
                tickets.length && (
                    <div className='ticket-pagination-container full flex-center'>
                        <Pagination />
                    </div>
                )
            }
        </Fragment>
    )
}