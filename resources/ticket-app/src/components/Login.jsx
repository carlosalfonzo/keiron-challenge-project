import React, { useRef } from 'react';
import './css/Login.css';
import Input from './common/Input';
import Button from './common/Button';
import Loader from './common/Loader';
import Axios from '../utils/Axios';
import { Link } from 'react-router-dom';


export default function Login(props) {
  const {
    error,
    loginAttempt,
    history: {
      push
    },
    loading
  } = props;
  const inputValues = useRef({});

  function getInputChange(inputName, value) {
    inputValues.current = {
      ...inputValues.current,
      [inputName]: value
    }
  }

  function attempToLogin() {
    if (!loading) {
      const request = new Axios({
        method: 'post',
        url: 'login',
        data: {
          ...inputValues.current
        }
      }).send();
      request.then(
        (reponse) => {
          // save token in all the requests instances
          Axios.authToken = reponse.token;
        }
      )
      return loginAttempt(request);
    }
  }

  return (
    <div className='login-view-wrapper full flex-center'>
      <div className='login-form-container border-radius second-background border flex wrap'>
        <p className='full bold login-title higthlight-color'>Log In</p>
        <Input className='full' onChange={getInputChange} name='email' label='Email' />
        <Input className='full' onChange={getInputChange} name='password' label='Password' password />
        <Button className='login-button' onClick={attempToLogin} text='Log In' />
        {
          loading && <Loader />
        }
        {
          error && <p className='full higthlight-color login-message'>{error}</p>
        }
        <Link className='full login-register-link overflow-ellipsis' to='/register'>Register a new User</Link>
      </div>
    </div>
  );
}