import React, { useEffect, useState, useRef } from 'react';
import SelectUser from '../containers/SelectUserContainer';
import SelectTicketQuantity from './SelectTicketQuantity';
import './css/NewTicket.css';
import Axios from '../utils/Axios';
import Loader from './common/Loader';
import { goTo } from '../routes/SetAppRoutes';
import Button from './common/Button';

export default function NewTicket(props) {
    const {
        loading,
        createTickets,
        error,
        message
    } = props;
    const [step, setStep] = useState(1);
    const data = useRef({
        user_id: null,
        selected_name: null,
        quantity: null
    });

    function saveUser({ user_id, selected_name }) {
        data.current = {
            ...data.current,
            user_id,
            selected_name
        };
    }

    function attemptCreateTicket(quantity) {
        if (!loading) {
            data.current = {
                ...data.current,
                quantity
            };
            const request = new Axios({
                url: 'ticket',
                method: 'post',
                data: {
                    ...data.current
                }
            });
            createTickets(request.send());
        }
    }

    function setStepToRender() {
        switch (step) {
            case 1:
                return <SelectUser
                    onSelectedUser={saveUser}
                    nextStep={() => setStep(2)}
                />;
            case 2:
                return <SelectTicketQuantity
                    selectedName={data.current.selected_name}
                    onConfirmed={attemptCreateTicket}
                    prevStep={() => setStep(1)}
                />;
        }
    }

    return (
        <div className='full flex-center wrap'>
            <p className='new-tickets-title full higthlight-color bold'>New Ticket</p>
            {setStepToRender()}
            {
                loading && <Loader />
            }
            {
                (message || error) && <p className='new-ticket-message full higthlight-color'>{message ? message : error}</p>
            }
            {
                message && (
                    <Button className='success-go-home-button' onClick={() => goTo('/tickets')} text='Go Home' />
                )
            }
        </div>
    );
}