import React, { useRef, useState } from 'react';
import Input from './common/Input';
import Button from './common/Button';

export default function SelectTicketQuantity(props) {
    const {
        onConfirmed,
        prevStep,
        selectedName
    } = props;
    const quantity = useRef(undefined);
    const [confirm, toggleConfirm] = useState(false);

    function getInputChange(_, value) {
        quantity.current = value;
        if (confirm) {
            toggleConfirm(false);
        }
    }

    function showConfirmation() {
        if (quantity.current && quantity.current > 0 && !confirm) {
            toggleConfirm(true);
        }
    }

    function sendConfirmation() {
        return onConfirmed(quantity.current)
    }

    return (
        <div className='select-quantity-container full flex wrap'>
            <div className='quantity-input-container full flex wrap border-radius border'>
                <p className='full step-title bold'>Select Ticket Quantity</p>
                <Input
                    className='full'
                    onChange={getInputChange}
                    name='quantity'
                    label='Tickets Number'
                    numeric
                />
                <div className='ok-button-container flex-center full'>
                    <Button onClick={showConfirmation} text='Ok' />
                </div>
            </div>
            {
                confirm && (
                    <div className='confirmation-container flex-center wrap'>
                        <p className='new-confirmation-message'>{`Are you sure you want to create ${quantity.current} ${quantity.current > 1 ? 'tickets' : 'ticket'} to ${selectedName}?`}</p>
                        <Button onClick={sendConfirmation} text='Confirm & create' />
                    </div>
                )
            }
            <Button onClick={prevStep} text='Go Prev' />
        </div>
    )
}