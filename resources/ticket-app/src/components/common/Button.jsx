import React from 'react';
import '../css/button.css';

export default function Button(props) {
  const {
    onClick,
    children,
    className,
    text
  } = props;

  return (
    <div className={`border-radius flex button border strong-background ${className ? className : ''}`} onClick={onClick}>
      {
        text ? <p className='overflow-ellipsis higthlight-color'>{text}</p> : { children }
      }
    </div>
  )
}