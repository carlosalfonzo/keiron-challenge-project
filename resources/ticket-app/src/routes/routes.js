import { lazy } from 'react';

// components
const NotFound = lazy(() => import('../components/NotFound'));
const Login = lazy(() => import('../containers/LoginContainer'));
const Register = lazy(() => import('../containers/RegisterContainer'));
const UserTicketsView = lazy(() => import('../containers/UserTicketsViewContainer'));
const AdminTicketsView = lazy(() => import('../containers/AdminTicketsViewContainer'));
const NewTicket = lazy(() => import('../containers/NewTicketContainer'));

const notFound = [{
  key: 'not-found',
  component: NotFound
}];

const publicRoutes = [
  {
    exact: true,
    path: '/',
    key: 'Login',
    component: Login
  },
  {
    exact: true,
    path: '/register',
    key: 'register',
    component: Register
  },
];

export const routes = {
  public: [
    ...publicRoutes,
    ...notFound
  ],
  user: [
    {
      exact: true,
      path: [
        '/',
        '/tickets',
        '/tickets/:page',
      ],
      key: 'My-Tickets',
      component: UserTicketsView
    },
    ...notFound,
  ],
  administrator: [
    {
      exact: true,
      path: [
        '/',
        '/tickets',
        '/tickets/:page',
      ],
      key: 'all-Tickets',
      component: AdminTicketsView
    },
    {
      exact: true,
      path: '/create',
      key: 'new-ticket',
      component: NewTicket
    },
    ...notFound
  ]
};
export default routes;