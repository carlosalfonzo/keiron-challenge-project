import { connect } from 'react-redux';
import TicketList from '../components/TicketList';
import { withRouter } from 'react-router-dom';

export default withRouter(
    connect(
        state => {
            return {
                tickets: state.ticketsReducer.tickets
            }
        },
    )(TicketList)
);