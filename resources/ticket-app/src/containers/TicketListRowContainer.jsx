import { connect } from 'react-redux';
import TicketListRow from '../components/TicketListRow';
import { deleteTicket } from '../redux/ticketsActions';

export default connect(
    __RouterContext => {
        return {}
    },
    {
        deleteTicket,
    }
)(TicketListRow)