import { connect } from 'react-redux';
import NewTicket from '../components/NewTicket';
import { withRouter } from 'react-router-dom';
import { createTickets } from '../redux/ticketsActions';

export default withRouter(
    connect(
        state => {
            return {
                loading: state.ticketsReducer.loading,
                message: state.ticketsReducer.message,
                error: state.ticketsReducer.errorCreating,
            }
        },
        {
            createTickets
        }
    )(NewTicket)
);