import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import UserTicketsView from '../components/UserTicketsView';
import {
    requestTickets,
    userRequestForTicket
} from '../redux/ticketsActions';

export default withRouter(
    connect(
        state => {
            return {
                loading: state.ticketsReducer.loading,
                requestingTicket: state.ticketsReducer.loadingNewTicket,
                error: state.ticketsReducer.error,
            }
        },
        {
            requestTickets,
            userRequestForTicket
        }
    )(UserTicketsView)
);