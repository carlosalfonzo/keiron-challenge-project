import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import AdminTicketsView from '../components/AdminTicketsView';
import { requestTickets } from '../redux/ticketsActions';

export default withRouter(
    connect(
        state => {
            return {
                loading: state.ticketsReducer.loading,
                refreshable: state.ticketsReducer.refreshable,
                error: state.ticketsReducer.error,
            }
        },
        {
            requestTickets,
        }
    )(AdminTicketsView)
);