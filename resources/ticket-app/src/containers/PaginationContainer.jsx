import { connect } from 'react-redux';
import Pagination from '../components/common/Pagination';
import { withRouter } from 'react-router-dom';

export default withRouter(
    connect(
        (state) => {
            return {
                paginate: state.ticketsReducer.pagination
            }
        }
    )(Pagination)
);