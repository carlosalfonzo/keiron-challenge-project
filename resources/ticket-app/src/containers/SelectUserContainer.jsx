import { connect } from 'react-redux';
import SelectUser from '../components/SelectUser';
import { requestUsers } from '../redux/usersActions';

export default connect(
    state => {
        return {
            loading: state.usersReducer.loading,
            users: state.usersReducer.users,
            error: state.usersReducer.error,
        }
    },
    {
        requestUsers
    }
)(SelectUser);