import { actions as authActions } from './authActions';
import { actions } from './usersActions';
import { suffixes } from './store';

const initialState = {
	loading: false,
	error: null,
	users: []
};

function usersReducer(state = initialState, action) {
	switch (action.type) {
		case actions.GET_USERS + suffixes.START:
			return {
				...state,
				loading: true,
				error: null
			}
		case actions.GET_USERS + suffixes.ERROR:
			return {
				...state,
				loading: false,
				error: true
			}
		case actions.GET_USERS + suffixes.SUCCESS:
			return {
				...state,
				loading: false,
				users: [
					...state.users,
					...action.payload
				]
			}
		case authActions.LOGOUT + suffixes.SUCCESS:
		case authActions.LOGOUT + suffixes.ERROR:
			return { ...initialState }
		default:
			return state;
	}
}
export default usersReducer;