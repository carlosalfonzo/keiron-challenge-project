import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { createPromise } from 'redux-promise-middleware';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
// apps reducers
import responsiveReducer from './responsiveReducer';
import authReducer from './authReducer';
import ticketsReducer from './ticketsReducer';
import usersReducer from './usersReducer';

export const suffixes = {
  START: '_START',
  SUCCESS: '_SUCCESS',
  ERROR: '_ERROR'
};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;

function configureStore() {
  const middlewares = [
    thunk,
    reduxImmutableStateInvariant(),
    createPromise({
      promiseTypeSuffixes: ['START', 'SUCCESS', 'ERROR'],
    }),
  ];
  return createStore(
    appCombineReducer(),
    {},
    (composeEnhancers && composeEnhancers(applyMiddleware(...middlewares))) || applyMiddleware(...middlewares)
  );
};

function appCombineReducer() {
  return (
    combineReducers({
      responsiveReducer,
      ticketsReducer,
      authReducer,
      usersReducer
    })
  )
}

export default configureStore;