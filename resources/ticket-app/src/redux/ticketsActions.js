// action types
export const actions = {
    GET_TICKETS: "GET_TICKETS",
    NEW_REQUESTED_TICKET: "NEW_REQUESTED_TICKET",
    DELETE: "DELETE",
    CREATE: "CREATE",
}

/**
 * Make tickets request
 *
 * @export
 * @param {*} axios
 * @returns
 */
export function requestTickets(axios) {
    return {
        type: actions.GET_TICKETS,
        payload: axios
    }
}

/**
 * User requests a new ticket
 *
 * @export
 * @param {*} axios
 * @returns
 */
export function userRequestForTicket(axios) {
    return {
        type: actions.NEW_REQUESTED_TICKET,
        payload: axios
    }
}

/**
 * Admin delete a ticket
 *
 * @export
 * @param {*} axios
 * @returns
 */
export function deleteTicket(axios, index) {
    return {
        type: actions.DELETE,
        payload: axios,
        meta: index
    }
}

/**
 * Admin create a ticket
 *
 * @export
 * @param {*} axios
 * @returns
 */
export function createTickets(axios) {
    return {
        type: actions.CREATE,
        payload: axios
    }
}