// action types
export const actions = {
    GET_USERS: "GET_USERS",
}

/**
 * Request all users
 *
 * @export
 * @param {*} axios
 * @returns
 */
export function requestUsers(axios) {
    return {
        type: actions.GET_USERS,
        payload: axios
    }
}