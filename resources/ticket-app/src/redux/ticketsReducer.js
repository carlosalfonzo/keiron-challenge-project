import { actions as authActions } from './authActions';
import { actions } from './ticketsActions';
import { updateArrayElementRedux } from '../utils/functions';
import moment from 'moment';
import { suffixes } from './store';

const initialState = {
	loading: false,
	loadingNewTicket: false,
	tickets: [],
	refreshable: false,
	errorCreating: false,
	pagination: {
		current: null,
		next: null,
		last: null
	},
};

function ticketsReducer(state = initialState, action) {
	switch (action.type) {
		case actions.GET_TICKETS + suffixes.START:
			return {
				...state,
				loading: true,
				error: null
			}
		case actions.GET_TICKETS + suffixes.ERROR:
			return {
				...state,
				loading: false,
				error: action.payload.message
			}
		case actions.GET_TICKETS + suffixes.SUCCESS:
			return {
				...state,
				loading: false,
				tickets: [
					...action.payload.data
				],
				refreshable: false,
				pagination: {
					current: action.payload.current_page,
					next: action.payload.current_page <= action.payload.last_page ? parseInt(action.payload.current_page) + 1 : null,
					last: action.payload.last_page
				}
			}
		case actions.DELETE + suffixes.START:
			return {
				...state,
				tickets: state.tickets.map((element, index) => {
					if (index === action.meta) {
						return {
							...state.tickets[action.meta],
							loading: true,
							message: null,
							error: false
						}
					}
					return element;
				})
			}
		case actions.DELETE + suffixes.ERROR:
			return {
				...state,
				tickets: state.tickets.map((element, index) => {
					if (index === action.meta) {
						return {
							...state.tickets[action.meta],
							loading: false,
							error: true
						}
					}
					return element;
				})
			}
		case actions.DELETE + suffixes.SUCCESS:
			return {
				...state,
				tickets: state.tickets.map((element, index) => {
					if (index === action.meta) {
						return {
							...state.tickets[action.meta],
							loading: false,
							message: action.payload.message
						}
					}
					return element;
				}),
				refreshable: true,
			}
		case actions.NEW_REQUESTED_TICKET + suffixes.START:
			return {
				...state,
				loadingNewTicket: true,
			}
		case actions.NEW_REQUESTED_TICKET + suffixes.ERROR:
			return {
				...state,
				loadingNewTicket: false,
				error: action.payload.message
			}
		case actions.NEW_REQUESTED_TICKET + suffixes.SUCCESS:
			let new_tickets = state.tickets;
			if (new_tickets.length === 10) new_tickets.pop();
			return {
				...state,
				loadingNewTicket: false,
				tickets: [
					{
						requested_ticket: true,
						created_at: moment().format(),
						id: 'New'
					},
					...new_tickets
				]
			}
		case actions.CREATE + suffixes.START:
			return {
				...state,
				loading: true,
				error: null,
			}
		case actions.CREATE + suffixes.ERROR:
			return {
				...state,
				loading: false,
				errorCreating: action.payload.message ? action.payload.message : 'An error ocurred, quantity must be least than 21',
			}
		case actions.CREATE + suffixes.SUCCESS:
			return {
				...state,
				loading: false,
				message: action.payload.message
			}
		case authActions.LOGOUT + suffixes.SUCCESS:
		case authActions.LOGOUT + suffixes.ERROR:
			return { ...initialState }
		default:
			return state;
	}
}
export default ticketsReducer;