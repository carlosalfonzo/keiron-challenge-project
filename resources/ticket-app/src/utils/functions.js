import moment from 'moment';

/**
 * Save Session in Local Storage
 *
 * @export
 */
export function saveSessionInLocalStorage(userData) {
  return localStorage.setItem('ticket-user', JSON.stringify(userData));
}

/**
 * Get user session from Local Storage
 *
 * @export
 */
export function getSessionFromLocalStorage() {
  return JSON.parse(localStorage.getItem('ticket-user'));
}

/**
 * Delete user session from Local Storage
 *
 * @export
 */
export function clearSessionFromLocalStorage() {
  return localStorage.removeItem('ticket-user');
}

export function formatTimeStamp(timestamp) {
  return moment(timestamp).fromNow();
}