export const localAxiosRoute = 'http://localhost:8080/keiron-challenge-project/public/api/';
export const productionAxiosRoute = 'http://api.zz.com.ve/api/';
export const appEnv = 'env';

export const userKinds = {
  user: 'user',
  administrator: 'administrator',
};