<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'user_kind_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    public function userKind()
    {
        return $this->belongsTo(UserKind::class);
    }

    public function setPasswordAttribute($new_password)
    {
        $this->attributes['password'] = bcrypt($new_password);
    }

    public function isUserKind()
    {
        return $this->userKind->name === UserKind::USERS_KINDS[1];
    }

    public function isAdminKind()
    {
        return $this->userKind->name === UserKind::USERS_KINDS[0];
    }
}
