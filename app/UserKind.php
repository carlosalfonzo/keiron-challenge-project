<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserKind extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public const USERS_KINDS = [
        'administrator',
        'user'
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function scopeGetUserKindId($query)
    {
        return $query->where('name', 'user')->first()->id;
    }
}
