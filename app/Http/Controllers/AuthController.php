<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use App\UserKind;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if (!$request->has('email') || !$request->has('password')) {
            return response()->json([
                'message' => 'Missing email or password'
            ], 400);
        }
        $user = User::with('userkind')
            ->where('email', $request->email)
            ->first();
        /* not found email */
        if (!$user) {
            return response()->json([
                'message' => 'User not found, sign up!'
            ], 400);
        }
        /* make login attemp */
        $loged_user = Auth::attempt([
            'email' => $request->email,
            'password' => $request->password
        ]);
        if ($loged_user) {
            return response()->json([
                'user' => new UserResource($user),
                'token' => 'Bearer ' . $user->createToken('ticket api token')->accessToken
            ], 200);
        }
        return response()->json([
            'message' => 'Wrong email and password combination'
        ], 400);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logedout'
        ], 200);
    }

    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email',
            'name' => 'required|min:4|max:30',
            'password' => 'required|min:6|max:64'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Wrong Email, Name or Password',
                'errors' => $validator->errors()
            ], 400);
        }
        $user = new User([
            'email' => $request->email,
            'name' => $request->name,
            'password' => $request->password,
            'user_kind_id' => UserKind::getUserKindId()
        ]);
        $user->save();
        return response()->json([
            'message' => 'Successfully created User, now you need to login'
        ], 200);
    }
}
