<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\Http\Resources\TicketResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pagination = $request->has('paginate') ? $request->paginate : 10;
        $ticket = null;
        if ($request->user()->isUserKind()) {
            $ticket = Ticket::where('user_id', $request->user()->id)->orderBy('created_at', 'DESC');
        } else {
            $ticket = Ticket::with('user')->orderBy('created_at', 'DESC');
        }
        return response()->json(
            new TicketResource(
                $ticket->paginate($pagination)
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->user()->isAdminKind()) {
            return response()->json([
                'message' =>  'Unauthenticated.'
            ], 401);
        }
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'quantity' => 'nullable|numeric|integer|min:1|max:21'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'missing or wrong user id'
            ], 400);
        }
        $user = User::find($request->user_id);
        $quantity = $request->has('quantity') ? $request->quantity : 1;
        for ($i = 0; $i < $quantity; $i++) {
            $user->tickets()->create([
                'requested_ticket' => false
            ]);
        }
        return response()->json([
            'message' => 'successfully created tokens'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ticket $ticket)
    {
        if (!$request->user()->isAdminKind()) {
            return response()->json([
                'message' =>  'Unauthenticated.'
            ], 401);
        }
        if (!$ticket) {
            return response()->json([
                'message' =>  'wrong ticket id'
            ], 400);
        }
        $validator = Validator::make($request->all(), [
            'new_user_id' => 'required|exists:users,id'
        ]);
        if ($validator->fails() || $ticket->user_id == $request->new_user_id) {
            return response()->json([
                'message' => 'Wrong new user id'
            ], 400);
        }
        $ticket->user_id = $request->new_user_id;
        $ticket->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(ticket $ticket)
    {
        $ticket->delete();
        return response()->json([
            'message' => 'successfully deleted ticket'
        ], 200);
    }

    /**
     * Request new ticket by user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function requestNewTicker(Request $request)
    {
        if (!$request->user()->isUserKind()) {
            return response()->json([
                'message' =>  'Unauthenticated.'
            ], 401);
        }
        $request->user()->tickets()->create([
            'requested_ticket' => true
        ]);
        return response()->json([
            'message' => 'successfully requested token'
        ], 200);
    }
}
