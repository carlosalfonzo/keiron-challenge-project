# ENGLISH

Hello, to setup this project in local environment, you need to do the following steps:

1. Run
> composer install
2. Config your database setting to the app **(.env file)**
    You can use a postgresDB or MySqlDB, just change the DB driver
3. Run
> php artisan key:generate
4. Run
> php artisan passport:install
5. Run
> php artisan migrate:fresh --seed
6. Run
> php artisan passport:client --personal 

You can leave blank name for client
7. All users has "**secret**" as password stored. Admin user email is *testAdmin*
8. Run
> cd resources/ticket-app/
9. Run
> npm install
10. Config your API local base url for all clients requests in *utils/config.js* **(remember to write /api/ at end)**
11. Run
> npm start
12. **Congrats!** you're running

### P.S

If you want to make a production client build, run *npm run build* and upload the *build* directory files online.


# ESPAÑOL

Hola, para configurar este proyecto en un entorno local, necesitas seguir los siguientes pasos:

1. Ejecuta 
> composer install
2. Configura tus ajustes para la base de datos **(archivo .env)**
Puedes usar una base de datos postgresDB o MySqlDB, solo necesitas cambiar el driver de DB
4. Ejecuta 
> php artisan passport:install
5. Ejecuta 
> php artisan migrate:fresh --seed
6. Ejecuta 
> php artisan passport:client --personal

Puedes darle a enter con un nombre de cliente en blanco

7. Todos los usuarios tienen como contarseña "**secret**". El correo del usuario admin es *testAdmin*
8. Ejecuta 
> cd resources/ticket-app/
9. Ejecuta 
> npm install
10. Configura tu url base para todas las peticiones a la API desde el cliente en *utils/config.js* **(Recuerda añadir /api/ al final de la ruta)**
11. Ejecuta 
> npm start
12. **Felicidades**! Estás listo.

### P.D

Si quieres hacer un build de producción, ejecuta *npm run build* y sube todos los achivos del directorio *build* en linea.