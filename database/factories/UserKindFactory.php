<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UserKind;
use Faker\Generator as Faker;

$factory->define(UserKind::class, function (Faker $faker) {
    return [
        'name' => 'User'
    ];
});
