<?php

use Illuminate\Database\Seeder;

class AppSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* creating administrator user's kind */
        factory(App\UserKind::class, 1)->create([
            'name' => 'administrator'
        ]);
        /* creating user user's kind */
        factory(App\UserKind::class, 1)->create([
            'name' => 'user'
        ]);
        /* creating administrator test user */
        factory(App\User::class, 1)->create([
            'email' => 'testAdmin',
            'user_kind_id' => App\UserKind::where('name', 'administrator')->first()->id
        ]);
        dump('testAdmin user created');
        /* creating user test user */
        factory(App\User::class, 1)->create([
            'email' => 'testuser',
            'user_kind_id' => App\UserKind::where('name', 'user')->first()->id
        ])->each(function ($user) {
            factory(App\Ticket::class, 10)->create([
                'user_id' => $user->id
            ]);
        });
        dump('testuser user created');
        /* creating user organic users */
        factory(App\User::class, 5)->create([
            'user_kind_id' => App\UserKind::where('name', 'user')->first()->id
        ])->each(function ($user) {
            factory(App\Ticket::class, 10)->create([
                'user_id' => $user->id
            ]);
        });
    }
}
